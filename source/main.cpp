#include "penta/controller/game.h"
#include "penta/model/game_state.h"
#include "penta/gui/gui.h"
#include "penta/controller/human_player.h"
#include "penta/controller/computer_player.h"

using namespace penta;

int main(int, char**)
{
   std::unique_ptr<GameState> upGameState = std::make_unique<GameState>(19, 19);
   std::unique_ptr<Gui> upGui = std::make_unique<Gui>();
   //std::unique_ptr<IPlayer> upCirclePlayer = std::make_unique<ComputerPlayer>(upGameState.get(), 4u, SearchMode::adjecent);
   std::unique_ptr<IPlayer> upRectanglePlayer = std::make_unique<ComputerPlayer>(upGameState.get(), 2u, SearchMode::adjecent);
   std::unique_ptr<IPlayer> upCirclePlayer = std::make_unique<HumanPlayer>(upGameState.get(), upGui.get());
   //std::unique_ptr<IPlayer> upRectanglePlayer = std::make_unique<HumanPlayer>(upGameState.get(), upGui.get());

   Game game(
      std::move(upGameState),
      std::move(upGui),
      std::move(upCirclePlayer),
      std::move(upRectanglePlayer));

   while (true)
   {
      using namespace std::chrono_literals;
      std::this_thread::sleep_for(30ms);
      game.update();
      if (game.quit())
      {
         return 0;
      }
   }

   return 0;
}