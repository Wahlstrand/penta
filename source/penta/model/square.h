#pragma once

#include "penta/model/team.h"

namespace penta
{
   struct Square
   {
      Square();

      Team m_owner;
   };
}