#include "penta/model/game_state.h"

#include "penta/model/square.h"
#include "penta/model/move.h"

namespace penta
{
   namespace
   {
      std::vector<Coordinate> winningLine(GameState const& state)
      {
         return std::vector<Coordinate>();
      }
      struct GameWinFinder
      {
         GameWinFinder()
            : m_isWon(false)
            , m_streak(0)
            , m_streakTeam(Team::none)
         {
         }
         void directionBegin()
         {
            m_streak = 0;
            m_streakTeam = Team::none;
         }
         void visit(uint32_t row, uint32_t col, Square const& square)
         {
            if (m_isWon)
            {
               return;
            }

            if (square.m_owner != m_streakTeam)
            {
               m_streakTeam = square.m_owner;
               m_streak = 1;
               m_winBegin.m_row = row;
               m_winBegin.m_col = col;
            }
            else
            {
               m_streak++;
               if (m_streak >= 5 && m_streakTeam != Team::none)
               {
                  m_isWon = true;
                  m_winEnd.m_row = row;
                  m_winEnd.m_col = col;
               }
            }
         }
         void directionEnd()
         {
         }

         bool m_isWon;
         int32_t m_streak;
         Team m_streakTeam;
         Coordinate m_winBegin;
         Coordinate m_winEnd;
      };
      bool gameWon(GameState const& state, Coordinate& winBegin, Coordinate& winEnd)
      {
         GameWinFinder winFinder;
         traverseAllLines(winFinder, state.m_board);
         if (winFinder.m_isWon)
         {
            winBegin = winFinder.m_winBegin;
            winEnd = winFinder.m_winEnd;
         }
         return winFinder.m_isWon;
      }
   }

   GameState::GameState(int32_t rows, int32_t cols)
      : m_board(rows, cols)
      , m_activeTeam(Team::circle)
      , m_moves()
      , m_finished(false)
      , m_winner(Team::none)
   {
   }
   bool isFull(Matrix<Square> const& board)
   {
      for (int32_t row = 0; row < board.rows(); row++)
      {
         for (int32_t col = 0; col < board.cols(); col++)
         {
            if (board.element(Coordinate(row, col)).m_owner == Team::none)
            {
               return false;
            }
         }
      }
      return true;
   }
   bool isOccupied(GameState const& gameState, Coordinate const& coord)
   {
      return gameState.m_board.element(coord).m_owner != Team::none;
   }
   bool hasOccupiedNeighbour(GameState const& gameState, Coordinate const& coord, int32_t delta)
   {
      for (int32_t dRow = -delta; dRow <= delta; dRow++)
      {
         for (int32_t dCol = -delta; dCol <= delta; dCol++)
         {
            if (dRow == 0 && dCol == 0)
            {
               continue;
            }

            Coordinate coord(coord.m_row + dRow, coord.m_col + dCol);
            if (isValid(coord, gameState.m_board) && isOccupied(gameState, coord))
            {
               return true;
            }
         }
      }
      return false;
   }
   void makeMove(GameState& state, Coordinate const& coordinate)
   {
      state.m_moves.push(coordinate);
      state.m_board.element(coordinate).m_owner = state.m_activeTeam;

      if (gameWon(state, state.m_winBegin, state.m_winEnd))
      {
         state.m_finished = true;
         state.m_winner = state.m_activeTeam;
      }
      else if (isFull(state.m_board))
      {
         state.m_finished = true;
         state.m_winner = Team::none;
      }

      state.m_activeTeam = other(state.m_activeTeam);
   }
   void unmakeMove(GameState& state)
   {
      if (state.m_moves.empty())
      {
         throw std::runtime_error("Cannot back up game state, no moves have been done.");
      }

      state.m_board.element(state.m_moves.top()).m_owner = Team::none;
      state.m_activeTeam = other(state.m_activeTeam);
      state.m_finished = false;
      state.m_winner = Team::none;

      state.m_moves.pop();
   }
}