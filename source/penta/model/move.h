#pragma once

#include "penta/model/coordinate.h"

namespace penta
{
   struct Move
   {
      Move();
      Move(int32_t row, int32_t col);
      Move(Coordinate const& coord);
      Move(float eval, Coordinate const& coord);

      float m_eval;
      Coordinate m_coord;
   };
}