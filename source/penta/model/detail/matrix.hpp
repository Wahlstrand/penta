namespace penta
{
   template <typename ElementT>
   Matrix<ElementT>::Matrix(int32_t rows, int32_t cols, ElementT value)
      : m_elements(rows, std::vector<ElementT>(cols, value))
   {
   }
   template <typename ElementT>
   int32_t Matrix<ElementT>::rows() const
   {
      return m_elements.size();
   }
   template <typename ElementT>
   int32_t Matrix<ElementT>::cols() const
   {
      return m_elements[0].size();
   }
   template <typename ElementT>
   ElementT& Matrix<ElementT>::element(int32_t row, int32_t col)
   {
      return m_elements[row][col];
   }
   template <typename ElementT>
   ElementT const& Matrix<ElementT>::element(int32_t row, int32_t col) const
   {
      return m_elements[row][col];
   }
   template <typename ElementT>
   ElementT const& Matrix<ElementT>::element(Coordinate const& coord) const
   {
      return m_elements[coord.m_row][coord.m_col];
   }
   template <typename ElementT>
   ElementT& Matrix<ElementT>::element(Coordinate const& coord)
   {
      return m_elements[coord.m_row][coord.m_col];
   }
   template <typename ElementT>
   bool isValid(Coordinate const& coord, Matrix<ElementT> const& matrix)
   {
      return
         coord.m_col >= 0 &&
         coord.m_col < matrix.cols() &&
         coord.m_row >= 0 &&
         coord.m_row < matrix.rows();
   }
   template <typename FuncT, typename ElementT>
   void forEachElement(FuncT& func, Matrix<ElementT> const& matrix)
   {
      for (int32_t row = 0; row < matrix.rows(); row++)
      {
         for (int32_t col = 0; col < matrix.cols(); col++)
         {
            auto& element = matrix.element(Coordinate(row, col));
            func(row, col, element);
         }
      }
   }

   template <typename TraverserT, typename ElementT>
   void traverseRows(TraverserT& traverser, Matrix<ElementT> const& matrix)
   {
      for (int32_t row = 0; row < matrix.rows(); row++)
      {
         traverser.directionBegin();
         for (int32_t col = 0; col < matrix.cols(); col++)
         {
            traverser.visit(row, col, matrix.element(row, col));
         }
         traverser.directionEnd();
      }
   }
   template <typename TraverserT, typename ElementT>
   void traverseCols(TraverserT& traverser, Matrix<ElementT> const& matrix)
   {
      for (int32_t col = 0; col < matrix.cols(); col++)
      {
         traverser.directionBegin();
         for (int32_t row = 0; row < matrix.rows(); row++)
         {
            traverser.visit(row, col, matrix.element(row, col));
         }
         traverser.directionEnd();
      }
   }
   template <typename TraverserT, typename ElementT>
   void traverseDiagonal(
      TraverserT& traverser, 
      Matrix<ElementT> const& matrix, 
      Coordinate const& start, 
      int32_t dRow,
      int32_t dCol)
   {
      Coordinate current = start;
      traverser.directionBegin();
      while (isValid(current, matrix))
      {
         traverser.visit(current.m_row, current.m_col, matrix.element(current));
         current.m_row += dRow;
         current.m_col += dCol;
      }
      traverser.directionEnd();
   }
   template <typename TraverserT, typename ElementT>
   void traverseDiagonals(TraverserT& traverser, Matrix<ElementT> const& matrix)
   {
      auto rows = matrix.rows();
      for (int32_t startRow = 0; startRow < rows; startRow++)
      {
         Coordinate startCoord(startRow, 0);
         traverseDiagonal(traverser, matrix, startCoord, -1, 1);  // up-right
         traverseDiagonal(traverser, matrix, startCoord,  1, 1);  // down-right
      }
      for (int32_t startCol = 1; startCol < matrix.cols(); startCol++)
      {
         traverseDiagonal(traverser, matrix, Coordinate(rows - 1, startCol), -1, 1);  // up-right
         traverseDiagonal(traverser, matrix, Coordinate(0,        startCol),  1, 1);  // down-right
      }
   }
   template <typename TraverserT, typename ElementT>
   void traverseAllLines(TraverserT& traverser, Matrix<ElementT> const& matrix)
   {
      traverseRows(traverser, matrix);
      traverseCols(traverser, matrix);
      traverseDiagonals(traverser, matrix);
   }
}
