#include "penta/model/team.h"

#include <stdexcept>

namespace penta
{
   Team other(Team team)
   {
      switch (team)
      {
      case Team::circle:
         return Team::rectangle;
      case Team::rectangle:
         return Team::circle;
      }
      throw std::runtime_error("Invalid conversion");
   }
}