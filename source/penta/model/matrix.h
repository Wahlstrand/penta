#pragma once

#include <cstdint>
#include <vector>

#include "penta/model/coordinate.h"

namespace penta
{
   template <typename ElementT>
   class Matrix
   {
   public:
      Matrix(int32_t rows, int32_t cols, ElementT value = ElementT());

      int32_t rows() const;
      int32_t cols() const;

      ElementT const& element(int32_t row, int32_t col) const;
      ElementT& element(int32_t row, int32_t col);
      ElementT const& element(Coordinate const& coord) const;
      ElementT& element(Coordinate const& coord);

   private:
      std::vector<std::vector<ElementT>> m_elements; //[row][col]
   };
   
   template <typename ElementT>
   bool isValid(Coordinate const&, Matrix<ElementT> const&);

   //FuncT should accept args (row, col, ElementT const&)
   template <typename FuncT, typename ElementT> 
   void forEachElement(FuncT&, Matrix<ElementT> const&);

   //TraverserT should have members
   //void directionBegin();
   //void visit(uint32_t row, uint32_t col, ElementT const&));
   //void directionEnd();
   template <typename TraverserT, typename ElementT>
   void traverseRows(TraverserT&, Matrix<ElementT> const&);
   template <typename TraverserT, typename ElementT>
   void traverseCols(TraverserT&, Matrix<ElementT> const&);
   template <typename TraverserT, typename ElementT>
   void traverseDiagonals(TraverserT&, Matrix<ElementT> const&);
   
   //Combines all above
   template <typename TraverserT, typename ElementT>
   void traverseAllLines(TraverserT&, Matrix<ElementT> const&);
}

#include "penta/model/detail/matrix.hpp"
