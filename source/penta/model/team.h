#pragma once

namespace penta
{
   enum Team
   {
      none,
      rectangle,
      circle,
   };
   Team other(Team team);
}