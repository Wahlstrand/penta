#include "penta/model/coordinate.h"

using namespace penta;

Coordinate::Coordinate()
   : m_row()
   , m_col()
{
}

Coordinate::Coordinate(int32_t row, int32_t col)
   : m_row(row)
   , m_col(col)
{
}

namespace penta
{
   bool operator==(Coordinate const& c1, Coordinate const& c2)
   {
      return
         c1.m_row == c2.m_row &&
         c1.m_col == c2.m_col;
   }
}