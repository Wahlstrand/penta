#include "penta/model/move.h"

using namespace penta;

Move::Move()
   : m_eval()
   , m_coord()
{
}
Move::Move(int32_t row, int32_t col)
   : m_eval()
   , m_coord(row, col)
{
}
Move::Move(Coordinate const& coord)
   : m_eval()
   , m_coord(coord)
{
}
Move::Move(float eval, Coordinate const& coord)
   : m_eval(eval)
   , m_coord(coord)
{
}