#pragma once

#include <cstdint>
#include <vector>
#include <stack>

#include "penta/model/matrix.h"
#include "penta/model/square.h"
#include "penta/model/coordinate_fwd.h"
#include "penta/model/team.h"

namespace penta
{
   struct GameState
   {
      GameState(int32_t rows, int32_t cols);

      Matrix<Square> m_board;
      Team m_activeTeam;

      //TODO: Move these elsewhere?
      bool m_finished;
      std::stack<Coordinate> m_moves;
      Team m_winner;             //Only valid when finished is set to true
      Coordinate m_winBegin;     //Only valid when finished is set to true
      Coordinate m_winEnd;       //Only valid when finished is set to true
   };

   bool isFull(Matrix<Square> const& board);
   bool isOccupied(GameState const&, Coordinate const&);
   bool hasOccupiedNeighbour(GameState const&, Coordinate const&, int32_t delta);

   void makeMove(GameState&, Coordinate const&);
   void unmakeMove(GameState&);
}