#pragma once

#include <cstdint>

namespace penta
{
   struct Coordinate
   {
      Coordinate();
      Coordinate(int32_t row, int32_t col);
      int32_t m_row;
      int32_t m_col;
   };

   bool operator==(Coordinate const& c1, Coordinate const& c2);
}