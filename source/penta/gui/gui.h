#pragma once

#include <memory>
#include <optional>

#include "penta/model/game_state_fwd.h"
#include "penta/model/matrix_fwd.h"
#include "penta/model/square_fwd.h"
#include "penta/model/coordinate.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

namespace penta
{
   struct Icons
   {
      Icons();
      ::sf::RectangleShape m_boardSquare;
      ::sf::RectangleShape m_rectangleMarker;
      ::sf::CircleShape m_circleMarker;
   };

   struct Colors
   {
      Colors();
      ::sf::Color m_background;
      ::sf::Color m_boardSquare;
      ::sf::Color m_boardSquareHighlighted;
      ::sf::Color  m_boardSquareWinning;
      ::sf::Color m_rectangleMarker;
      ::sf::Color m_circleMarker;
   };

   struct UserInput
   {
      UserInput();
      bool m_quit;

      //TODO: may need a list here..?
      std::optional<Coordinate> m_selectedCoord;
   };

   class Gui
   {
   public:
      Gui();
      void update(GameState const& state);
      UserInput const& input() const;
   
   private:
      std::unique_ptr<::sf::RenderWindow> m_upWindow;
      ::sf::View m_view;
      UserInput m_input;
      Icons m_icons;
      Colors m_colors;

      void resetInput();
      void processEvents();
      void updateView(Matrix<Square> const& board);
      void drawBoard(GameState const& board);
      void drawMarkers(Matrix<Square> const& board);
      ::sf::Vector2f mapToScene(::sf::Vector2i const& mousePosDesktop);
      Coordinate mapToCoord(::sf::Vector2i const& mousePosDesktop);
   };
}