#include "penta/gui/gui.h"

#include <iostream>
#include <cmath>

#include "penta/model/game_state.h"
#include "penta/model/square.h"
#include "penta/model/coordinate.h"

#include <SFML/Window/Event.hpp>

using namespace penta;

static float const boardSquareWidth = 0.95f;
static float const squareMarkerWidth = 0.5f;
static float const circleMarkerRadius = 0.3f;

namespace {
   float distance(Coordinate const& c1, Coordinate const& c2)
   {
      float dRow = static_cast<float>(c1.m_row - c2.m_row);
      float dCol = static_cast<float>(c1.m_col - c2.m_col);
      return sqrt(dRow * dRow + dCol * dCol);
   }
   bool isOnWinningLine(Coordinate const& coord, Coordinate const& winBegin, Coordinate const& winEnd)
   {
      static float const epsilon = 0.01f;
      float delta = distance(coord, winBegin) + distance(coord, winEnd) - distance(winBegin, winEnd);
      return delta < epsilon;
   }
   void paintBoard(
      GameState const& state,
      Icons& icons,
      Colors const& colors,
      ::sf::RenderWindow* pWindow, 
      ::sf::Vector2f const& mousePosScene)
   {
      for (int32_t row = 0; row < state.m_board.rows(); row++)
      {
         for (int32_t col = 0; col < state.m_board.cols(); col++)
         {
            float posX = static_cast<float>(col);
            float posY = static_cast<float>(row);

            Coordinate coord(row, col);
            icons.m_boardSquare.setPosition(posX, posY);
            if (icons.m_boardSquare.getGlobalBounds().contains(mousePosScene))
            {
               icons.m_boardSquare.setFillColor(colors.m_boardSquareHighlighted);
            }
            else if (state.m_finished &&
               state.m_winner != Team::none &&
               isOnWinningLine(coord, state.m_winBegin, state.m_winEnd))
            {
               icons.m_boardSquare.setFillColor(colors.m_boardSquareWinning);
            }
            else if (!state.m_moves.empty() && state.m_moves.top() == coord)
            {
               icons.m_boardSquare.setFillColor(colors.m_boardSquareHighlighted);
            }
            else
            {
               icons.m_boardSquare.setFillColor(colors.m_boardSquare);
            }
            pWindow->draw(icons.m_boardSquare);
         }
      }
   }
   class MarkerPainter
   {
   public:
      MarkerPainter(Icons& icons, Colors const& colors, ::sf::RenderWindow* pWindow)
         : m_icons(icons)
         , m_colors(colors)
         , m_pWindow(pWindow)
      {
      }
      void operator()(int32_t row, int32_t col, Square const& square)
      {
         switch (square.m_owner)
         {
         case Team::none:
            break;
         case Team::rectangle:
            drawMarker(m_icons.m_rectangleMarker, m_colors.m_rectangleMarker, row, col);
            break;
         case Team::circle:
            drawMarker(m_icons.m_circleMarker, m_colors.m_circleMarker, row, col);
            break;
         }
      }
   private:
      Icons & m_icons;
      Colors const& m_colors;
      ::sf::RenderWindow* m_pWindow;

      void drawMarker(::sf::Shape& shape, ::sf::Color color, size_t row, size_t col)
      {
         float posX = static_cast<float>(col);
         float posY = static_cast<float>(row);
         shape.setPosition(posX, posY);
         shape.setFillColor(color);
         m_pWindow->draw(shape);
      }
   };
}

Icons::Icons()
   : m_boardSquare(::sf::Vector2f(boardSquareWidth, boardSquareWidth))
   , m_rectangleMarker(::sf::Vector2f(squareMarkerWidth, squareMarkerWidth))
   , m_circleMarker(circleMarkerRadius)
{
   m_boardSquare.setOrigin(0.5f * m_boardSquare.getSize());
   m_rectangleMarker.setOrigin(0.5f * m_rectangleMarker.getSize());
   m_circleMarker.setOrigin(circleMarkerRadius, circleMarkerRadius);
}
Colors::Colors()
   : m_background(63, 91, 166)
   , m_boardSquare(254, 235, 201)
   , m_boardSquareHighlighted(243, 202, 162)
   , m_boardSquareWinning(191, 228, 118)
   , m_rectangleMarker(11, 183, 214)
   , m_circleMarker(226, 117, 137)
{
}
UserInput::UserInput()
   : m_quit(false)
   , m_selectedCoord()
{
}
Gui::Gui()
   : m_upWindow(new ::sf::RenderWindow(::sf::VideoMode(960, 960), "Penta", ::sf::Style::Default, ::sf::ContextSettings(24U, 8U, 16U)))
   , m_view()
   , m_input()
{
}
void Gui::update(GameState const& game)
{
   resetInput();
   processEvents();
   m_upWindow->clear(m_colors.m_background);
   updateView(game.m_board);
   drawBoard(game);
   drawMarkers(game.m_board);
   m_upWindow->display();
}
UserInput const& Gui::input() const
{
   return m_input;
}
void Gui::resetInput()
{
   m_input.m_selectedCoord.reset();
}
void Gui::processEvents()
{
   ::sf::Event event;
   while (m_upWindow->pollEvent(event))
   {
      switch (event.type)
      {
      case ::sf::Event::Closed:
         m_input.m_quit = true;
         break;
      case ::sf::Event::KeyPressed:
         if (event.key.code == ::sf::Keyboard::Q)
         {
            m_input.m_quit = true;
         }
         break;
      case ::sf::Event::KeyReleased:
         break;
      case ::sf::Event::MouseMoved:
         break;
      case ::sf::Event::MouseButtonPressed:
      {
         if (event.mouseButton.button == ::sf::Mouse::Button::Left)
         {
            //y is row dir
            //x is col dir
            auto coord = mapToCoord(::sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
            m_input.m_selectedCoord = coord;
         }
         break;
      }
      default:
         break;
      }
   }
}
void Gui::updateView(Matrix<Square> const& game)
{
   //     _____>x 
   //     |
   //     |
   //   y v

   //Sizing
   float const boardWidth = static_cast<float>(game.cols() - 1);
   float const boardHeight = static_cast<float>(game.rows() - 1);
   float const paddingWidth = 2.0f;
   float const paddingHeight = 2.0f;

   float const viewLeft = -paddingWidth;
   float const viewTop = -paddingHeight;
   float const viewWidth = boardWidth + 2.0f * paddingWidth;
   float const viewHeight = boardHeight + 2.0f * paddingHeight;
   m_view = ::sf::View(::sf::FloatRect(viewLeft, viewTop, viewWidth, viewHeight));
   m_upWindow->setView(m_view);

}
void Gui::drawBoard(GameState const& state)
{
   auto mousePosScene = mapToScene(::sf::Mouse::getPosition(*m_upWindow));
   paintBoard(state, m_icons, m_colors, m_upWindow.get(), mousePosScene);
}
void Gui::drawMarkers(Matrix<Square> const& board)
{
   MarkerPainter painter(m_icons, m_colors, m_upWindow.get());
   forEachElement(painter, board);
}
::sf::Vector2f Gui::mapToScene(::sf::Vector2i const& mousePosDesktop)
{
   return m_upWindow->mapPixelToCoords(mousePosDesktop, m_view);
}

Coordinate Gui::mapToCoord(::sf::Vector2i const& mousePosDesktop)
{
   //y is row dir
   //x is col dir
   auto scenePos = mapToScene(mousePosDesktop);

   return Coordinate(
      std::lround(scenePos.y),
      std::lround(scenePos.x));
}
