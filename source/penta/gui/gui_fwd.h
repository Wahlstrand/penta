#pragma once

namespace penta
{
   struct Icons;
   struct Colors;
   struct UserInput;
   class Gui;
}