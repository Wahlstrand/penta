#include "penta/controller/computer_player.h"

#include <iostream>
#include <array>
#include <cmath>
#include <algorithm>

#include "penta/model/game_state.h"
#include "penta/model/node.h"
#include "penta/model/move.h"

using namespace penta;

namespace {
   std::vector<Move> allMoves(GameState const& state)
   {
      std::vector<Move> result;
      result.reserve(state.m_board.cols() * state.m_board.rows()); //Review this if memory becomes an issue

      for (int32_t row = 0; row < state.m_board.rows(); row++)
      {
         for (int32_t col = 0; col < state.m_board.cols(); col++)
         {
            if (!isOccupied(state, Coordinate(row, col)))
            {
               result.emplace_back(row, col);
            }
         }
      }
      return result;
   }
   std::vector<Move> neighbouringMoves(GameState const& state, int32_t delta)
   {
      std::vector<Move> result;
      result.reserve(state.m_board.cols() * state.m_board.rows()); //Review this if memory becomes an issue

      for (int32_t row = 0; row < state.m_board.rows(); row++)
      {
         for (int32_t col = 0; col < state.m_board.cols(); col++)
         {
            Coordinate coord(row, col);
            if (!isOccupied(state, coord) && hasOccupiedNeighbour(state, coord, delta))
            {
               result.emplace_back(row, col);
            }
         }
      }
      return result;
   }
   std::vector<Move> findMoves(GameState const& state, SearchMode mode)
   {
      if (state.m_finished)
      {
         return std::vector<Move>();
      }

      if (state.m_moves.empty())
      {
         //Always all in this case
         return allMoves(state);
      }

      switch (mode)
      {
      case SearchMode::all:
         return allMoves(state);
      case SearchMode::adjecent:
         return neighbouringMoves(state, 1);
      default:
         throw std::runtime_error("Unknown SearchMode");
      }
   }

   //These need tweaking!
   namespace evaluation_constants
   {
      float const center_distance_value = -0.01f;
      //float const win = std::numeric_limits<float>::max();
      float const win = 100'000'000;
      std::array<float, 6> const open =      { 0.0f,     2.0f,   500.0f,    5'000.0f,  500'000.0f,  win }; //0->5
      std::array<float, 6> const half_open = { 0.0f,     1.0f,    10.0f,      300.0f,   50'000.0f,  win }; //0->5
      std::array<float, 6> const closed =    { 0.0f,     0.0f,     0.0f,        0.0f,        0.0f,  win }; //0->5
   }
   float streakValue(std::array<float, 6> const& values, Team team, uint32_t streak)
   {
      streak = std::min(streak, 5u);
      switch (team)
      {
      case Team::none:
         return 0.0f;
      case Team::circle:
         return values[streak];
      case Team::rectangle:
         return -1.0f * values[streak];
      }
      throw std::runtime_error("Failed to compute streak value");
   }
   float open(Team team, uint32_t count)
   {
      return streakValue(evaluation_constants::open, team, count);
   }
   float halfOpen(Team team, uint32_t count)
   {
      return streakValue(evaluation_constants::half_open, team, count);
   }
   float closed(Team team, uint32_t count)
   {
      return streakValue(evaluation_constants::closed, team, count);
   }
  
   struct StreakEvaluator
   {
      StreakEvaluator()
         : m_openStart(false)
         , m_streak(0)
         , m_streakTeam(Team::none)
         , m_eval(0.0f)
      {
      }
      void directionBegin()
      {
         m_openStart = false;
         m_streak = 0;
         m_streakTeam = Team::none;
      }
      void visit(uint32_t, uint32_t, Square const& square)
      {
         auto occupant = square.m_owner;
         if (occupant == m_streakTeam)
         {
            //Ongoing streak
            m_streak++;
            return;
         }
        
         if (m_streakTeam == Team::none)
         {
            //Streak of teamless squares ended
            m_openStart = m_streak > 0u; //if streak is 0 we're at the edge of the board
            m_streakTeam = occupant;
            m_streak = 1;
            return;
         }
         bool openEnd = occupant == Team::none;
         if (openEnd && m_openStart)
         {
            m_eval += open(m_streakTeam, m_streak);
         }
         else if (openEnd || m_openStart)
         {
            m_eval += halfOpen(m_streakTeam, m_streak);
         }
         else
         {
            m_eval += closed(m_streakTeam, m_streak);
         }
         m_streak = 1;
         m_streakTeam = occupant;
         m_openStart = openEnd;
      }
      void directionEnd()
      {
         if (m_openStart)
         {
            m_eval += halfOpen(m_streakTeam, m_streak);
         }
         else
         {
            m_eval += closed(m_streakTeam, m_streak);
         }
      }

      bool m_openStart;
      uint32_t m_streak;
      Team m_streakTeam;
      float m_eval;
   };
   struct DistanceAccumulator //Distance is positive for circle
   {
      DistanceAccumulator(Matrix<Square> const& board)
         : m_centerRow(0.5f * board.rows() - 0.5f)
         , m_centerCol(0.5f * board.cols() - 0.5f)
         , m_distance(0.0f)
      {
      }
      void operator()(int32_t row, int32_t col, Square const& square)
      {
         switch (square.m_owner)
         {
         case Team::none:
            return;
         case Team::circle:
            m_distance += distanceToCenter(row, col);
            return;
         case Team::rectangle:
            m_distance -= distanceToCenter(row, col);
            return;
         }
      }
      float distanceToCenter(int32_t row, int32_t col)
      {
         return sqrtf(
            std::pow(static_cast<float>(row) - m_centerRow, 2) +
            std::pow(static_cast<float>(col) - m_centerCol, 2));
      }

      float const m_centerRow;
      float const m_centerCol;
      float m_distance;
   };
   float eval(GameState const& state)
   {
      StreakEvaluator streakEvaluator;
      traverseAllLines(streakEvaluator, state.m_board);

      DistanceAccumulator accumulator(state.m_board);
      forEachElement(accumulator, state.m_board);

      return
         streakEvaluator.m_eval + 
         evaluation_constants::center_distance_value * accumulator.m_distance;
   }
   std::vector<Move> eval(GameState& state, std::vector<Move>&& moves)
   {
      for (auto&& move : moves)
      {
         makeMove(state, move.m_coord);
         move.m_eval = eval(state);
         unmakeMove(state);
      }
      return std::move(moves);
   }
   bool greater(Move const& m1, Move const& m2)
   {
      return m1.m_eval > m2.m_eval;
   }
   bool smaller(Move const& m1, Move const& m2)
   {
      return m1.m_eval < m2.m_eval;
   }
   std::vector<Move> order(GameState const& state, std::vector<Move>&& moves)
   {
      if (state.m_activeTeam == Team::circle)
      {
         std::sort(moves.begin(), moves.end(), greater);
      }
      else
      {
         std::sort(moves.begin(), moves.end(), smaller);
      }
      return std::move(moves);
   }



   Move alphaBeta(GameState& state, uint32_t depth, SearchMode mode, float alpha, float beta)
   {
      if (depth == 0)
      {
         return Move(eval(state), Coordinate());
      }
      auto moves = findMoves(state, mode);
      if (moves.empty())
      {
         return Move(eval(state), Coordinate());
      }

      bool const isMaximizing = state.m_activeTeam == Team::circle;

      Move best(
         isMaximizing ? -std::numeric_limits<float>::max() : std::numeric_limits<float>::max(),
         Coordinate());

      moves = eval(state, std::move(moves));
      moves = order(state, std::move(moves));

      for (auto&& move : moves)
      {
         makeMove(state, move.m_coord);
         move.m_eval = alphaBeta(state, depth - 1, mode, alpha, beta).m_eval;
         unmakeMove(state);


         if (isMaximizing)
         {
            best = std::max(best, move, smaller);
            alpha = std::max<float>(alpha, best.m_eval);
         }
         else
         {
            best = std::min(best, move, smaller);
            alpha = std::min<float>(alpha, best.m_eval);
         }

         if (alpha >= beta)
         {
            return best;
         }
      }
      return best;
   }
   Move computeMove(GameState state, uint32_t depth, SearchMode mode)
   {
      auto start = std::chrono::system_clock::now();
      auto move = alphaBeta(state, depth, mode, -std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
      auto end = std::chrono::system_clock::now();

      auto elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

      std::cout
         << "m_board.element(" << move.m_coord.m_row << ", " << move.m_coord.m_col << ").m_owner = "
         << (state.m_activeTeam == Team::circle ? "Team::circle" : "Team::rectangle") << "; "
         << "//d: " << depth
         << "    e: " << move.m_eval 
         << "    t: " << elapsed_milliseconds.count() << " ms " << std::endl;
      return move;
   }
}

ComputerPlayer::ComputerPlayer(GameState const* pGameState, uint32_t searchDepth, SearchMode searchMode)
   : m_pGameState(pGameState)
   , m_futureMove()
   , m_searchDepth(searchDepth)
   , m_searchMode(searchMode)
{
}
void ComputerPlayer::activate() const
{
   if (isFull(m_pGameState->m_board))
   {
      std::cout << "Board is full, computer won't find a move." << std::endl;
      return;
   }
   m_futureMove = std::async(std::launch::async, computeMove, *m_pGameState, m_searchDepth, m_searchMode);
}
bool ComputerPlayer::hasMove() const
{
   return 
      m_futureMove.valid() &&  
      m_futureMove.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready;
}
Move ComputerPlayer::move() const
{
   return m_futureMove.get();
}
