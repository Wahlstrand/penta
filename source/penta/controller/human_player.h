#pragma once

#include "penta/controller/iplayer.h"

#include "penta/model/game_state_fwd.h"
#include "penta/gui/gui_fwd.h"

namespace penta
{
   class HumanPlayer : public IPlayer
   {
   public:
      //IPlayer
      HumanPlayer(GameState const* pGameState, Gui const* pGui);
      void activate() const;
      bool hasMove() const;
      Move move() const;
      
   private:
      GameState const* m_pGameState;
      Gui const* m_pGui;
   };
}