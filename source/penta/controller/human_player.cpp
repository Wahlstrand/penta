#include "penta/controller/human_player.h"

#include "penta/model/game_state.h"
#include "penta/gui/gui.h"

#include <iostream>

using namespace penta;

HumanPlayer::HumanPlayer(GameState const* pGameState, Gui const* pGui)
   : m_pGameState(pGameState)
   , m_pGui(pGui)
{

}
void HumanPlayer::activate() const
{
}
bool HumanPlayer::hasMove() const
{
   if (!m_pGui->input().m_selectedCoord.has_value())
   {
      return false;
   }
   Coordinate inputCoord = m_pGui->input().m_selectedCoord.value();

   return 
      isValid(inputCoord, m_pGameState->m_board) &&
      !isOccupied(*m_pGameState, inputCoord);
}
Move HumanPlayer::move() const
{
   Move move(m_pGui->input().m_selectedCoord.value());

   std::cout
      << "m_board.element(" << move.m_coord.m_row << ", " << move.m_coord.m_col << ").m_owner = "
      << (m_pGameState->m_activeTeam == Team::circle ? "Team::circle" : "Team::rectangle") << "; " << std::endl;

   return move;
}