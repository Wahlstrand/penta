#pragma once

#include <memory>

#include "penta/model/game_state_fwd.h"
#include "penta/gui/gui_fwd.h"
#include "penta/controller/iplayer_fwd.h"

namespace penta
{
   class Game
   {
   public:
      Game(
         std::unique_ptr<GameState>&& upGameState,
         std::unique_ptr<Gui>&& upGui,
         std::unique_ptr<IPlayer>&& upCirclePlayer,
         std::unique_ptr<IPlayer>&& upRectanglePlayer);
      ~Game();

      void update();
      bool quit() const;

   private:
      std::unique_ptr<GameState> m_upGameState;
      std::unique_ptr<Gui> m_upGui;
      std::unique_ptr<IPlayer> m_upCirclePlayer;
      std::unique_ptr<IPlayer> m_upRectanglePlayer;
      IPlayer* m_pActivePlayer;
      bool m_quit;
   };
}