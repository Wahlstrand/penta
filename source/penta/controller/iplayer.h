#pragma once

#include "penta/model/move.h"

namespace penta
{
   class IPlayer
   {
   public:
      virtual ~IPlayer() {}
      virtual void activate() const = 0;     //Called when it is this players turn to move
      virtual bool hasMove() const = 0;      //Should return true when a move is available
      virtual Move move() const = 0;         //Only valid after hasMove() returns true.
                                             //After this method is called, the player no longer
                                             //has a move and must be activated again.
   };
}