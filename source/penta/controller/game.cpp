#include "penta/controller/game.h"

#include "penta/model/game_state.h"
#include "penta/gui/gui.h"
#include "penta/controller/iplayer.h"

using namespace penta;

Game::Game(
   std::unique_ptr<GameState>&& upGameState,
   std::unique_ptr<Gui>&& upGui,
   std::unique_ptr<IPlayer>&& upCirclePlayer,
   std::unique_ptr<IPlayer>&& upRectanglePlayer)
   : m_upGameState(std::move(upGameState))
   , m_upGui(std::move(upGui))
   , m_upCirclePlayer(std::move(upCirclePlayer))
   , m_upRectanglePlayer(std::move(upRectanglePlayer))
   , m_pActivePlayer(m_upGameState->m_activeTeam == Team::circle ? m_upCirclePlayer.get() : m_upRectanglePlayer.get())
   , m_quit(false)
{
   m_pActivePlayer->activate();
}

Game::~Game()
{
}

void Game::update()
{
   m_upGui->update(*m_upGameState);

   auto const& input = m_upGui->input();
   if (input.m_quit)
   {
      m_quit = true;
      return;
   }

   if (m_upGameState->m_finished)
   {
      return;
   }

   if (!m_pActivePlayer->hasMove())
   {
      return;
   }

   auto move = m_pActivePlayer->move();
   makeMove(*m_upGameState, move.m_coord);
   if (m_upGameState->m_finished)
   {
      return;
   }

   if (m_upGameState->m_activeTeam == Team::circle)
   {
      m_pActivePlayer = m_upCirclePlayer.get();
   }
   else
   {
      m_pActivePlayer = m_upRectanglePlayer.get();
   }

   m_pActivePlayer->activate();
}

bool Game::quit() const
{
   return m_quit;
}