#pragma once

#include <future>

#include "penta/controller/iplayer.h"

#include "penta/model/game_state_fwd.h"
#include "penta/model/move_fwd.h"

namespace penta
{
   enum SearchMode
   {
      all,
      adjecent
   };
   class ComputerPlayer : public IPlayer
   {
   public:
      //IPlayer
      ComputerPlayer(GameState const* pGameState, uint32_t searchDepth, SearchMode searchMode);
      void activate() const;
      bool hasMove() const;
      Move move() const;
      
   private:
      GameState const* m_pGameState;
      mutable std::future<Move> m_futureMove;
      uint32_t m_searchDepth;
      SearchMode m_searchMode;
   };
}